import React from "react";
import patient_profile from "../Images/patient-profile.jpg";
import "./Profile.css";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faEnvelope } from '@fortawesome/free-solid-scons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default function Profile() {
  return (
    <>
      <section className="doctor-dashboard">
        <div className="container heading">
          <h2>Dashboard</h2>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-3 profile-section">
              <div className="colour">
                {/* hello */}
                <img src={patient_profile} alt="" />
              </div>
              <h5>Dr : Smith Wright</h5>
              <h6> Hair Repair and Loss Expert</h6>
              <p>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error
                illo, iste voluptatum ipsam eius rerum illum facere reiciendis
                voluptatibus praesentium!
              </p>
              <div className="star-rating">
                {/* Five yellow stars */}
                <span className="star">&#9733;</span>
                <span className="star">&#9733;</span>
                <span className="star">&#9733;</span>
                <span className="star">&#9733;</span>
                <span className="star">&#9733;</span>
                <span className="rating">4.8</span>
              </div>
              <div class="email-div">
                {/* <i class="fa fa-envelope f-s-14 mr-10"></i> */}
                {/* <FontAwesomeIcon icon={faEnvelope} /> */}
                <FontAwesomeIcon icon="fa-regular fa-coffee" />
                <span>Email</span>
                <a href="/" class="blue-text">
                  <h5 class="text-info">smith-wright@example.com</h5>
                </a>
                <button
                  type="button"
                  class="btn btn-success send-message btn-icon btn-lg mt-10 right15"
                >
                  {/* <FontAwesomeIcon icon={['fa-brands', 'fa-telegram']} /> */}
&nbsp;{" "}
                  <span>Send Message</span>
                </button>
              </div>
            </div>
            <div className="col-md-9">{/* bye */}</div>
          </div>
        </div>
      </section>
    </>
  );
}
