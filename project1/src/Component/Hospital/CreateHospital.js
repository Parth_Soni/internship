import React from "react";
import { useState } from "react";
import "./Hospital.css";

export default function CreateHospital() {
  const [hospitalOwner, setHospitalOwner] = useState("");
  const [hospitalName, setHospitalName] = useState("");
  const [hospitalSite, setHospitalSite] = useState("");
  const [parentHospital, setParentHospital] = useState("");
  const [hospitalNumber, setHospitalNumber] = useState("");
  const [hospitalType, setHospitalType] = useState("");
  const [industry, setIndustry] = useState("");
  const [annualRevenue, setAnnualRevenue] = useState("");
  const [rating, setRating] = useState("");
  const [phone, setPhone] = useState("");
  const [fax, setFax] = useState("");
  const [website, setWebsite] = useState("");
  const [tickerSymbol, setTickerSymbol] = useState("");
  const [ownership, setOwnership] = useState("");
  const [employees, setEmployees] = useState("");
  const [sicCode, setSicCode] = useState("");
  const [billingStreet, setBillingStreet] = useState("");
  const [billingState, setBillingState] = useState("");
  const [billingCountry, setBillingCountry] = useState("");
  const [billingCity, setBillingCity] = useState("");
  const [billingCode, setBillingCode] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // Process the form data here
    console.log("Form submitted");
    console.log("Hospital Owner:", hospitalOwner);
    console.log("Hospital Name:", hospitalName);
    console.log("Hospital Site:", hospitalSite);
    console.log("Parent Hospital:", parentHospital);
    console.log("Hospital Number:", hospitalNumber);
    console.log("Hospital Type:", hospitalType);
    console.log("Industry:", industry);
    console.log("Annual Revenue:", annualRevenue);
    console.log("Rating:", rating);
    console.log("Phone:", phone);
    console.log("Fax:", fax);
    console.log("Website:", website);
    console.log("Ticker Symbol:", tickerSymbol);
    console.log("Ownership:", ownership);
    console.log("Employees:", employees);
    console.log("SIC Code:", sicCode);
    console.log("Billing Street:", billingStreet);
    console.log("Billing State:", billingState);
    console.log("Billing Country:", billingCountry);
    console.log("Billing City:", billingCity);
    console.log("Billing Code:", billingCode);
    console.log("Description:", description);
  };

  return (
    <>
      <section className="createHospital">
        
        <div className="container">
          <h4>Hospital Information</h4>
          <div className="row">
            {/* <form onSubmit={handleSubmit}> */}
            <div className="col-sm-6">
              <div className="form-group">
                <label htmlFor="hospitalOwner">Hospital Owner</label>
                <select
                  type="text"
                  id="hospitalOwner"
                  className="form-control"
                  value={hospitalOwner}
                  onChange={(e) => setHospitalOwner(e.target.value)}
                >
                  <option value=""></option>
                  <option value="">Two</option>
                  <option value="">Three</option>
                  </select>
              </div>
              <div className="form-group">
                <label htmlFor="hospitalName">Hospital Name</label>
                <input
                  type="text"
                  id="hospitalName"
                  className="form-control"
                  value={hospitalName}
                  onChange={(e) => setHospitalName(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="hospitalSite">Hospital Site</label>
                <input
                  type="text"
                  id="hospitalSite"
                  className="form-control"
                  value={hospitalSite}
                  onChange={(e) => setHospitalSite(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="parentHospital">Parent Hospital</label>
                <input
                  type="text"
                  id="parentHospital"
                  className="form-control"
                  value={parentHospital}
                  onChange={(e) => setParentHospital(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="hospitalNumber">Hospital Number</label>
                <input
                  type="text"
                  id="hospitalNumber"
                  className="form-control"
                  value={hospitalNumber}
                  onChange={(e) => setHospitalNumber(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="hospitalType">Hospital Type</label>
                <input
                  type="text"
                  id="hospitalType"
                  className="form-control"
                  value={hospitalType}
                  onChange={(e) => setHospitalType(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="industry">Industry</label>
                <input
                  type="text"
                  id="industry"
                  className="form-control"
                  value={industry}
                  onChange={(e) => setIndustry(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="annualRevenue">Annual Revenue</label>
                <input
                  type="text"
                  id="annualRevenue"
                  className="form-control"
                  value={annualRevenue}
                  onChange={(e) => setAnnualRevenue(e.target.value)}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <label htmlFor="rating">Rating</label>
                <input
                  type="text"
                  id="rating"
                  className="form-control"
                  value={rating}
                  onChange={(e) => setRating(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="phone">Phone</label>
                <input
                  type="text"
                  id="phone"
                  className="form-control"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="fax">Fax</label>
                <input
                  type="text"
                  id="fax"
                  className="form-control"
                  value={fax}
                  onChange={(e) => setFax(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="website">Website</label>
                <input
                  type="text"
                  id="website"
                  className="form-control"
                  value={website}
                  onChange={(e) => setWebsite(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="tickerSymbol">Ticker Symbol</label>
                <input
                  type="text"
                  id="tickerSymbol"
                  className="form-control"
                  value={tickerSymbol}
                  onChange={(e) => setTickerSymbol(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="ownership">Ownership</label>
                <input
                  type="text"
                  id="ownership"
                  className="form-control"
                  value={ownership}
                  onChange={(e) => setOwnership(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="employees">Employees</label>
                <input
                  type="text"
                  id="employees"
                  className="form-control"
                  value={employees}
                  onChange={(e) => setEmployees(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="sicCode">SIC Code</label>
                <input
                  type="text"
                  id="sicCode"
                  className="form-control"
                  value={sicCode}
                  onChange={(e) => setSicCode(e.target.value)}
                />
              </div>
            </div>
            {/* </form> */}
          </div>
        </div>
        <div className="container">
          <h4>Address Information</h4>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <label htmlFor="billingStreet">Billing Street</label>
                <input
                  type="text"
                  id="billingStreet"
                  className="form-control"
                  value={billingStreet}
                  onChange={(e) => setBillingStreet(e.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="billingState">Billing State</label>
                <br />
                <input
                  type="text"
                  id="billingState"
                  className="form-control"
                  value={billingState}
                  onChange={(e) => setBillingState(e.target.value)}
                />
              </div>
            </div>
            <div className="col-sm-6">
              <label htmlFor="billingCountry">Billing Country</label>
              <br />
              <input
                type="text"
                id="billingCountry"
                value={billingCountry}
                onChange={(e) => setBillingCountry(e.target.value)}
              />
                <br /> <div></div>
              <label htmlFor="billingCity">Billing City</label>
              <br />
              <input
                type="text"
                id="billingCity"
                value={billingCity}
                onChange={(e) => setBillingCity(e.target.value)}
              />
              <br />

              <label htmlFor="billingCode">Billing Code</label>
              <br /><input
                type="text"
                id="billingCode"
                value={billingCode}
                onChange={(e) => setBillingCode(e.target.value)}
              />
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <h4>Additional information</h4>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <textarea
                id="description"
                className="form-control"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              ></textarea>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <button className="submit_button" onClick={handleSubmit}>
              Submit
            </button>
          </div>
        </div>
      </section>
    </>
  );
}
