import React from 'react'
import "./Hospital.css"

export default function HospitalView() {
  return (
    <>
    <div className="container container-table">
        <div className="row row-table">
           <div className="data">
            <div>
                Hospital Owner
            </div>

            <div>
                Industry
            </div>

            <div>
                Employees
            </div>

            <div>
                Annual Revenue
            </div>
            <div>
                Phone
            </div>
           </div>
        </div>
    </div>

    <div className="container container-table">

        <div className="row row-table">
    <h4>Hospital Information</h4>
            <div className="data col-md-6">

            {/* <h4>Hide details</h4> */}
            <div>
                Hospital Owner
            </div>
            <div>
                Hospital Name
            </div>
            <div>
                Hospital Size
            </div>
            <div>
                parent hospital
            </div>
            <div>
                Hospital Type
            </div>
            <div>
                industry
            </div>
            <div>
                Annual revenue
            </div>
            <div>
                Created by 
            </div>

            
            </div>
            <div className="data col-md-6">
                <div>
                    rating
                </div>
                <div>
                    Phone No
                </div>
                <div>
                    Fax
                </div>
                <div>
                    Website
                </div>
                <div>
                    Ticker Symbol
                </div>
                <div>
                    Ownership
                </div>
                <div>
                    Employees
                </div>
                <div>
                    Sic Code
                </div>
                <div>
                    Modified by 
                </div>

            </div>
            <h5>Address information</h5>
            <div className="data col-md-6">
                <div>
                    Billing street
                </div>
                <div>
                    Billing state
                </div>
                <div>
                    Billing country
                </div>
            </div>
            <div className="col-md-6 data">
                <div>
                    Billing city
                </div>
                <div>
                    Billing code
                </div>
            </div>

            <h5>Description information</h5>
            <div className="col-md-6 data">
                <div>
                    Description 
                </div>
            </div>
            

        </div>

    </div>

    <div className="container container-table">
        <div className="row row-table">
            <h5> Notes</h5>
        </div>
    </div>
    <div className="container container-table">
        <div className="row row-table">
            <h5> Attachments</h5>
        </div>
    </div>
    <div className="container container-table">
        <div className="row row-table">
            <h5> Doctors</h5>
        </div>
    </div>
    </>
  )
}
