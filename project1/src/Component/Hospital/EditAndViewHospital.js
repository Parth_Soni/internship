import React from 'react'
import "../Doctor.js/Ediit&View.css"
import { Link } from 'react-router-dom';

export default function EditAndViewHospital() {
    const data = [
        {
          hospitalName: 'ABC Hospital',
          phone: '+1 123-456-7890',
          website: 'www.abchospital.com',
          hospitalOwner: 'Dr. Michael Johnson',
        },
        // Add more data objects as needed
    ];
    
    return (
    <>
    <div className="container container-table">
        <div className="row row-table">

         <table>
      <thead>
        <tr>
          <th>Hospital Name</th>
          <th>Phone</th>
          <th>Website</th>
          <th>Hospital Owner</th>
          <th>Edit</th>
          <th>View</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
          <tr key={index}>
            <td>{item.hospitalName}</td>
            <td>{item.phone}</td>
            <td>{item.website}</td>
            <td>{item.hospitalOwner}</td>
            <td>
              <Link to="/dashboard/EditHospital">
              <button>Edit</button>
              </Link>
            </td>
            <td>
              <Link to="/dashboard/ViewHospital">
              <button>View</button>
              </Link>
              </td>
          </tr>
        ))}
      </tbody>
    </table>
        </div>
    </div>
    </>
  )
}
