import React from "react";
import "./Log.css";
import lock from "../Images/signup.png";
import { useState } from "react"

export default function SignUp() {
  const [email, setEmail]=useState("")
  const [password, setPassword]=useState("")
  const [repeatPassword, setRepeatPassword]=useState("")
  const [emailStatus, setEmailStatus]=useState("")
  const [passwordStatus, setPasswordStatus]=useState("")
  const [repeatPasswordStatus, setRepeatPasswordStatus]=useState("")

  const emailValidate=()=>{
    
    // Regular expression to validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    
      if (email === '') {
        setEmailStatus('Email is required');
      } else if (!emailRegex.test(email)) {
        setEmailStatus('Invalid email format');
      } 
      else {
        setEmailStatus('');
      }
    
}


  const validatePassword = () => {
    if (password === '') {
      setPasswordStatus('Password is required');
    } else if (password.length < 8) {
      setPasswordStatus('Password should be at least 8 characters long');
    } 
    else {
      setPasswordStatus('');
    }
  };
  
  const validateRepeatPassword = () => {
    if (repeatPassword === '') {
      setRepeatPasswordStatus('Repeat password is required');
    } else if (password !== repeatPassword) {
      setRepeatPasswordStatus('Passwords do not match');
    } else {
      setRepeatPasswordStatus('');
    }
  };
  
  return (
  <>
      <section className="login">
        <div className="container container1">
          <div className="login-1">
            <img src={lock} alt="" className="img-lock" />
            <div className="login-text">
              <h5>Sign Up Now</h5>
              <p>Please enter your details to register</p>
            </div>
          </div>
          <div className="container-fluid login-form">
            <form action="">
              <div className="row">
                <div className="col-lg-6">
                  <div>
                    <label htmlFor="">  USER NAME</label>
                  </div>
                  <div>
                    <input type="text" placeholder="name" />
                  </div>
                  <div>
                    <label htmlFor="">EMAIL</label>
                  </div>
                  <div>
                    <input type="email"  placeholder=" email" onChange={(e)=>{setEmail(e.target.value)}} onBlur={emailValidate}/>
                  </div>
                  <div className="validation">{emailStatus}</div>
                </div>
                <div className="col-md-6">
                <div>
                    <label htmlFor="" >  PASSWORD</label>
                  </div>
                  <div>
                    <input type="text" placeholder="Password" onChange={(e)=>{setPassword(e.target.value)}} onBlur={validatePassword}/>
                  </div>
                  <div className="validation">{passwordStatus}</div>
                  <div>
                    <label htmlFor="">REPEAT PASSWORD</label>
                  </div>
                  <div>
                    <input type="email" placeholder="repeat password" onChange={(e)=>{setRepeatPassword(e.target.value)}} onBlur={validateRepeatPassword}/>
                  </div>
                  <div className="validation">{repeatPasswordStatus}</div>
                </div>
              </div>
            </form>
            <button className="button1">Login</button>
            <button className="button2">Sign up</button>
          </div>
          <div className="lowerground">

          <span className="span1">Forgot password?</span>
          <span className="span2">Sign up</span>
          </div>
        </div>
      </section>
    </>
  );
}
