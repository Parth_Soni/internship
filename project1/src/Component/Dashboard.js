import React from "react";
import logo from "../Images/burly.svg";
import { Link, useNavigate, Outlet } from "react-router-dom";
import "./Dashboard.css";
import Log from "./Log";
import doctor from "../Images/doctor.png";
import dashboard from "../Images/dashboard_logo.png";
import appointment from "../Images/appointment.png";
// import Doctor from './Doctors'
// import Appointment from './Appointment'
// import Dashboard_1 from './Dashboard_1'
// import Doctors_dashboard from './Doctors'
import profilepic from "../Images/profile.jpg";
import { useState, useEffect } from "react";
import profile5 from "../Images/5profile.png";
// import CreateHospital from './Hospital/CreateHospital'

export default function Dashboard() {
  const [name, setname] = useState("");

  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    setname("Roy");
  }, []);

  const navigate = useNavigate("/root");
  const Handlelogout = () => {
    // window.location.href="/logout"

    navigate("./log");
  };
  return (
    <>
      <nav className="navbar navbar-expand">
        <a href="/" className="navbar-brand">
          <img src={logo} alt="" />
        </a>
        <img src={profilepic} className="profile" alt="" />
        <li className="nav-item dropdown">
          <a
            className="nav-link dropdown-toggle"
            href="/"
            role="button"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            {name}
          </a>
          <ul className="dropdown-menu ">
            <li>
              <a className="dropdown-item" href="/">
                Setting
              </a>
            </li>
            <li>
              <a className="dropdown-item" href="/">
                Help
              </a>
            </li>
            <li>
              <Link className="dropdown-item" to="/dashboard/profile">
                Profile
              </Link>
            </li>
            

            <li>
              <hr className="dropdown-divider" />
            </li>
            <li>
              <a className="dropdown-item" href="/log" onClick={Handlelogout}>
                Logout
              </a>
            </li>
          </ul>
        </li>
      </nav>
      <main>
        <div className="container-fluid dashboard1">
          <div className="row">
            <div className="col-md-2">
              <h6 className="heading_dashboard"> MAIN</h6>
              <ul>
                <li>
                  <i>
                    <img src={dashboard} alt="" />
                  </i>
                  <Link to="/dashboard/dashboard1">
                    <span>Dashboard</span>
                  </Link>
                </li>
                
               
                <li>
                  <i>
                    <img src={appointment} alt="" />
                  </i>
                  <Link to="/dashboard/appointment">
                    <span>Appointments</span>
                  </Link>
                </li>
                <li>
                  <i>
                    <img src={profile5} alt="" />
                  </i>
                  <Link to="/dashboard/profile">
                    <span>Profile</span>
                  </Link>
                </li>
                <li>
                  <i>
                    <img src={profile5} alt="" />
                  </i>
                  <Link to="/dashboard/surgery">
                    <span>Surgery</span>
                  </Link>
                </li>
                <li>
                  <div className=" dropdown-sidebar">
                    <li className="" onClick={toggleDropdown}>
                      <li>
                        <i>
                          <img src={dashboard} alt="" />
                          &nbsp;
                          <span>Hospital &nbsp;</span>
                        </i>
                      </li>
                    </li>
                    {isOpen && (
                      <div className="dropdown-sidebar_link">
                        <div>
                          <Link to="/dashboard/hospital">Create</Link>
                        </div>
                        <div>
                          <Link to="/dashboard/EditAndViewHospital">Edit and View</Link>
                        </div>
                        
                      </div>
                    )}
                  </div>
                </li>
                <li>
                  <div className=" dropdown-sidebar">
                    <li className="" onClick={toggleDropdown}>
                      <li>
                        <i>
                          <img src={dashboard} alt="" />
                          &nbsp;
                          <span>Doctors &nbsp;</span>
                        </i>
                      </li>
                    </li>
                    {isOpen && (
                      <div className="dropdown-sidebar_link">
                        <div>
                          <Link to="/dashboard/doctors">Create</Link>
                        </div>
                        <div>
                          <Link to="/dashboard/Edit&ViewDoctor">Edit and View</Link>
                        </div>
                      </div>
                    )}
                  </div>
                </li>
              </ul>
            </div>
            <div className="col-md-10">
              {/* <Routes>

               <Route path='/dashboard/dashboard1' element={<Dashboard_1></Dashboard_1>}></Route>
                </Routes> */}
              {/* <Dashboard_1></Dashboard_1> */}
              <Outlet></Outlet>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}
