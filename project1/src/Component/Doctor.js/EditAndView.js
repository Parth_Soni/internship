import React from 'react'
import "../Hospital/Hospital.css"
import { Link } from 'react-router-dom'

export default function EditAndView() {
    const data = [
        {
          doctorName: 'John Doe',
          amount: '$1000',
          stage: 'In Progress',
          closingDate: '2023-06-30',
          hospitalName: 'ABC Hospital',
          contactName: 'Jane Smith',
          doctorOwner: 'Dr. Michael Johnson',
        },
    ]
    return (
  <>
  <div className="container container-table">
        <div className="row row-table">

    <table>
      <thead>
        <tr>
          <th>Doctor Name</th>
          <th>Amount</th>
          <th>Stage</th>
          <th>Closing Date</th>
          <th>Hospital Name</th>
          <th>Contact Name</th>
          <th>Doctor Owner</th>
          <th>Edit</th>
          <th>View</th>
        </tr>
      </thead>
      <tbody>
        {data.map((item, index) => (
            <tr key={index}>
            <td>{item.doctorName}</td>
            <td>{item.amount}</td>
            <td>{item.stage}</td>
            <td>{item.closingDate}</td>
            <td>{item.hospitalName}</td>
            <td>{item.contactName}</td>
            <td>{item.doctorOwner}</td>
            <td>
            <Link to="/dashboard/doctorView">
            <button>

             View
            </button>
            </Link> 
             </td>
          <td>
            <Link to="/dashboard/EditDoctor">
            <button>

            Edit
            </button>
            </Link>
          </td>
          </tr>
        ))}
      </tbody>
    </table>
        </div>
    </div>
  </>  
  )
}
