import React from "react";
import "../Doctors.css";
import { useState } from "react";
import "../Hospital/Hospital.css";
import "./Ediit&View.css";

export default function Doctors() {
  const [doctorOwner, setDoctorOwner] = useState("");
  const [doctorName, setDoctorName] = useState("");
  const [hospitalName, setHospitalName] = useState("");
  const [type, setType] = useState("");
  const [nextStep, setNextStep] = useState("");
  const [leadSource, setLeadSource] = useState("");
  const [contactName, setContactName] = useState("");
  const [amount, setAmount] = useState("");
  const [closingDate, setClosingDate] = useState("");
  const [stage, setStage] = useState("");
  const [pipeline, setPipeline] = useState("");
  const [probability, setProbability] = useState("");
  const [expectedRevenue, setExpectedRevenue] = useState("");
  const [campaignSource, setCampaignSource] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    // Process the form data here
    console.log("Form submitted");
    console.log("Doctor Owner:", doctorOwner);
    console.log("Doctor Name:", doctorName);
    console.log("Hospital Name:", hospitalName);
    console.log("Type:", type);
    console.log("Next Step:", nextStep);
    console.log("Lead Source:", leadSource);
    console.log("Contact Name:", contactName);
    console.log("Amount:", amount);
    console.log("Closing Date:", closingDate);
    console.log("Stage:", stage);
    console.log("Pipeline:", pipeline);
    console.log("Probability:", probability);
    console.log("Expected Revenue:", expectedRevenue);
    console.log("Campaign Source:", campaignSource);
    console.log("Description:", description);
  };

  return (
    <>
      <section className="doctor-dashboard createHospital">
        <div className="container heading">
          <h2>Doctors dashboard</h2>
        </div>
        
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="doctorOwner">Doctor Owner</label>
                <input type="text" id="doctorOwner" className="form-control" onChange={(e)=>{setDoctorOwner(e.target.value)}}/>
              </div>
              <div className="form-group">
                <label htmlFor="doctorName">Doctor Name</label>
                <input type="text" id="doctorName" className="form-control" onChange={(e)=>{setDoctorName(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="hospitalName">Hospital Name</label>
                <input type="text" id="hospitalName" className="form-control" onChange={(e)=>{setHospitalName(e.target.value);}} />
              </div>
              <div className="form-group">
                <label htmlFor="type">Type</label>
                <input type="text" id="type" className="form-control" onChange={(e)=>{setType(e.target.value);}} />
              </div>
              <div className="form-group">
                <label htmlFor="nextStep">Next Step</label>
                <input type="text" id="nextStep" className="form-control" onChange={(e)=>{setNextStep(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="leadSource">Lead Source</label>
                <input type="text" id="leadSource" className="form-control" onChange={(e)=>{setLeadSource(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="contactName">Contact Name</label>
                <input type="text" id="contactName" className="form-control" onChange={(e)=>{setProbability(e.target.value);}} />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="amount">Amount</label>
                <input type="text" id="amount" className="form-control" onChange={(e)=>{setAmount(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="closingDate">Closing Date</label>
                <input type="text" id="closingDate" className="form-control" onChange={(e)=>{setPipeline(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="stage">Stage</label>
                <input type="text" id="stage" className="form-control" onChange={(e)=>{setStage(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="pipeline">Pipeline</label>
                <input type="text" id="pipeline" className="form-control" onChange={(e)=>{setPipeline(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="probability">Probability (%)</label>
                <input type="text" id="probability" className="form-control" onChange={(e)=>{setProbability(e.target.value);}}/>
              </div>
              <div className="form-group">
                <label htmlFor="expectedRevenue">Expected Revenue</label>
                <input
                  type="text"
                  id="expectedRevenue"
                  className="form-control"
                  onChange={(e)=>{setExpectedRevenue(e.target.value)}}
                />
              </div>
              <div className="form-group">
                <label htmlFor="campaignSource">Campaign Source</label>
                <input
                  type="text"
                  id="campaignSource"
                  className="form-control"
                  onChange={(e)=>{setCampaignSource(e.target.value);}}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input type="text" id="description" className="form-control" onChange={(e)=>{setDescription(e.target.value);}}/>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <button className="submit_button" onClick={handleSubmit}>Submit</button>
          </div>
        </div>
      </section>
    </>
  );
}
