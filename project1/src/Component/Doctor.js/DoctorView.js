import React from 'react'
import "../Hospital/Hospital.css"

export default function DoctorView() {
  return (
    <>
        <div className="container container-table">
          <div className="row row-table">
            <div className="data">
              <div>
                Doctor Owner
              </div>
              <div>
                Stage
              </div>
              <div>
                Probability
              </div>
              <div>
                Expected Revenue
              </div>
              <div>
                Closing date
              </div>
            </div>
          </div>
        </div>
        <div className="container container-table">
          <div className="row row-table">
            <div className='data'>
              <h4>Contact Person</h4>
            </div>
          </div>
        </div>
        <div className="container container-table">
          <div className="row row-table">
              <h4>Doctor Information</h4>
            <div className="data col-md-6">
              <div>
                doctor owner
              </div>
              <div>
                Doctor name
              </div>
              <div>
                Hospital Name
              </div>
              <div>
                Type
              </div>
              <div>
                Next step
              </div>
              <div>
                Lead Source
              </div>
              <div>
                Contact Name
              </div>
              <div>
                Modified by
              </div>
            </div>
            <div className="data col-md-6">
              <div>
                Amount
              </div>
              <div>
                Closing Date
              </div>
              <div>
                Stage
              </div>
              <div>
                Pipeline
              </div>
              <div>
                Probability
              </div>
              <div>
                Expected Revenue
              </div>
              <div>
                Campaign source
                </div>
                <div>
                  Created by
                </div>
            </div>
            <h4>Description information</h4>
            <div className="data">
              Description
            </div>
          </div>
        </div>
        <div className="container container-table">
          <div className="row row-table">
            <h4>Notes</h4>
            <div className="data">

            </div>
          </div>
        </div>
        <div className="container container-table">
          <div className="row row-table">
            <h4>Attachments</h4>
            <div className="data">
              
            </div>
          </div>
        </div>
    </>
  )
}
