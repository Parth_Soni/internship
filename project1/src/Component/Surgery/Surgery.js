import React, { useEffect } from 'react'
import detail from "./surgery.json"
import { useState } from 'react'
import "./surgery.css"
import { Link } from 'react-router-dom'


export default function Surgery() {
    const [data, setData]=useState([]);

    useEffect(() => {
        
        const TName=sessionStorage.getItem("tName");
        console.log("Tbame is ",TName);
        try{
            // console.log("hello here")
            // console.log(typeof detail)
            // console.log("detail is", detail)
            // setData(detail)

            const filteredData = detail.filter((item) => item.treatmentName === TName);
            setData(filteredData);
            // console.log(data)
            // console.log("data us ",data)
            if(data.length===0)
            {
                console.log("one")
            }
        }catch(error)
        {
            console.error(error);
        
        
        }

    }, [10])

    
    
  return (
    <>
    <section className="doctor-dashboard surgery-detail">
        <div className="container heading">
            <h2>Surgery details</h2>
        </div>
    
    
        {data.map(d=>(
            <div key={d.treatmentName} >
                <div className="container heading">
        
        <div className="row">

            {/* <div>{d.treatmentName}</div> */}
            <div>
                <h6> Treatment Name - {d.treatmentName}</h6>

            </div>
            <div>
                <h6>
                    Desc - {d.desc}
                </h6>
            </div>
            <div>
                <h5>
                    Causes
                </h5>
                <div className="row">

                <div className="col-md-6 name">
                    <div>
                        Name - {d.causes[0]}
                    </div>
                    <div>
                        Name  - {d.causes[1]}
                    </div>
                     <div>
                        Name - {d.causes[2]}
                    </div>
                </div>
                <div className="col-md-6 icon" >
                    <div>
                        Icon - <img src={d.icon} alt="" />
                    </div>
                    <div>
                        Icon - <img src={d.icon} alt="" />
                    </div>
                    <div>
                        Icon - <img src={d.icon} alt="" />
                    </div>
                </div>
                </div>
                <div>
                    <h5>
                        Risks
                    </h5>
                </div>

                <div>
                    <Link to="/dashboard/SurgeryEdit">
                    <button className='submit_button'>Edit</button>
                    </Link>
                </div>
            </div>
            </div>
            </div>
    </div>
        ))}

            
            
           


    
    
    </section>
    </>
  )
}
