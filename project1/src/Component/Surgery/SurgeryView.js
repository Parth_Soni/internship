import React from 'react'
import { Link } from 'react-router-dom'
import detail from "./surgery.json"
import { useEffect } from 'react'
import { useState } from 'react'

export default function SurgeryView() {
  const [data,setData]=useState([])
    useEffect(() => {
    setData(detail)
  
    
  }, [10])
  
  const handleClick=(one)=>{
    sessionStorage.setItem("tName",one);
  }
    return (
    <>
    <section className="surgery_display">

    <div className="container container-table">
        <div className="row row-table">
            <table>
                <thead>
                    <tr>
                        <th>Treatment Name</th>
                        <th>        Desc</th>
                        <th>        View</th>
                    </tr>
                    
                            {data.map(d=>(
                                <tbody key={d.treatmentName}>
                                    <tr>
                                        <td>{d.treatmentName}</td>
                                        <td>{d.desc}</td>
                                    <td>
                                        <Link to="/dashboard/surgeryDetails">
                                        <button className='submit_button' onClick={handleClick(d.treatmentName)}>
                                                View
                                        </button>
                                        </Link>
                                    </td>

                                    </tr>
                                </tbody>
                            ))}
                    
                </thead>
            </table>
        </div>
    </div>
    </section>
    </>
  )
}
