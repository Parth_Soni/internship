import React from 'react'
import "./Log.css"
import lock from "../Images/lock.png"
import SignUp from './SignUp'
import { useNavigate } from 'react-router-dom'
import { useState } from "react"
import  logo  from "../Images/burly.svg"

export default function Log() {
    const [ email, setEmail]=useState("");
    const [password, setpassword]=useState("")
    const [status, setstatus]=useState("")
    const [emailStatus, setEmailStatus]=useState("")
    const [ passwordStatus, setPasswordStatus]=useState("")
    const navigate=useNavigate("/root");

    const gotosignup=()=>{
        navigate('/signup')
    }
    const emailValidate=()=>{
       
            // Regular expression to validate email format
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            
            if (email === '') {
              setEmailStatus('Email is required');
            } else if (!emailRegex.test(email)) {
              setEmailStatus('Invalid email format');
            } 
            else {
              setEmailStatus('');
            }
          
    }

      
        const validatePassword = () => {
          if (password === '') {
            setPasswordStatus('Password is required');
          } else if (password.length < 8) {
            setPasswordStatus('Password should be at least 8 characters long');
          } 
          else {
            setPasswordStatus('');
          }
        };
      

    const gotodashboard=()=>{
        if(email=="parth@gmail.com" && password=="Parth@123")
        {
            navigate('/dashboard')
            sessionStorage.setItem("email",email);
        }
        else{
            console.log(email)
            console.log(password)
            setstatus("wrong username or password")
        }

    }
    return (
        <section className="login">
            <div className="container container1">
                <div className="login-1">

                    <img src={lock}
                        alt=""
                        className='img-lock'/>
                    <div className="login-text">

                        <h5>Login Now!</h5>
                        <p>Please enter your credentials to login</p>
                    </div>
                </div>
                <div className="container-fluid login-form">
                    <div className="logo-section">
                        <img src={logo} alt="" />
                    </div>
                    <form action="">
                        <div>

                            <label htmlFor="">Email</label>
                        </div>
                        <div>

                            <input type="email" onChange={(e)=>{
                                setEmail(e.target.value);
                            }} placeholder='Enter your name'
                            onBlur={emailValidate}/>
                            <div className='validation'>{emailStatus}</div>
                        </div>
                        <div>

                            <label htmlFor="" >Password</label>
                        </div>
                        <div>

                            <input type="password" onChange={(e)=>{setpassword(e.target.value)}} placeholder='Enter your password' onBlur={validatePassword}/>
                            <div className='validation'>{passwordStatus}</div>
                        </div>
                        <span>{status}</span>

                    </form>
                    <button className='button1' onClick={gotodashboard}>Login</button>
                    <button className='button2' onClick={gotosignup}>Sign up</button>
                </div>
                <div className="lowerground">

                <span className="span1">Forgot password?</span>
                <span className="span2" onClick={gotosignup}>Sign up</span>
                </div>
            </div>
        </section>
    )
}
