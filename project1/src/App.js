// import logo from './logo.svg';
import './App.css';
import Dashboard from './Component/Dashboard';
import Log from './Component/Log';
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import SignUp from './Component/SignUp';
import Dashboard_1 from './Component/Dashboard_1';
// import Dashboard_1 from './Component/Dashboard_1';
import Doctors_dashboard from './Component/Doctor.js/Doctors';
import Appointment from './Component/Appointment';
import Profile from './Component/Profile';
import CreateHospital from './Component/Hospital/CreateHospital';
import EditAndView from './Component/Doctor.js/EditAndView';
import EditAndViewHospital from './Component/Hospital/EditAndViewHospital';
import HospitalView from './Component/Hospital/HospitalView';
import DoctorView from './Component/Doctor.js/DoctorView';
import EditHospital from './Component/Hospital/EditHospital';
import EditDoctor from './Component/Doctor.js/EditDoctor';
import Surgery from './Component/Surgery/Surgery';
import SurgeryView from './Component/Surgery/SurgeryView';
import SurgeryEdit from './Component/Surgery/SurgeryEdit';

function App() {
  return (
   <>
   <BrowserRouter>
   <Routes>
    <Route path="/" element={<Log></Log>}></Route>
    <Route path="/dashboard" element={<Dashboard></Dashboard>}>
      <Route path='/dashboard/dashboard1' element={<Dashboard_1></Dashboard_1>}/>
      <Route path='/dashboard/doctors' element={<Doctors_dashboard></Doctors_dashboard>}/>
      <Route path='/dashboard/appointment' element={<Appointment></Appointment>}> </Route>
      <Route path='/dashboard/profile' element={<Profile></Profile>}></Route>
      <Route path='/dashboard/hospital' element={<CreateHospital></CreateHospital>}></Route>
    <Route path='/dashboard/Edit&ViewDoctor' element={<EditAndView></EditAndView>}></Route>
    <Route path='/dashboard/EditAndViewHospital' element={<EditAndViewHospital></EditAndViewHospital>}></Route>
    <Route path='/dashboard/ViewHospital' element={<HospitalView></HospitalView>}></Route>
    <Route path='/dashboard/doctorView' element={<DoctorView></DoctorView>}></Route>
    <Route path='/dashboard/EditHospital' element={<EditHospital></EditHospital>}></Route>
    <Route path='/dashboard/EditDoctor' element={<EditDoctor></EditDoctor>}></Route>
    <Route path='/dashboard/surgery' element={<SurgeryView></SurgeryView>}></Route>
    <Route path='/dashboard/surgeryDetails' element={<Surgery></Surgery>}></Route>
    <Route path='/dashboard/surgeryEdit' element={<SurgeryEdit></SurgeryEdit>}></Route>
    </Route>
    <Route path="/log" element={<Log></Log>} ></Route>
    <Route path="/signup" element={<SignUp></SignUp>} ></Route>
    {/* <Route path="dashboard1" element={<Dashboard_1></Dashboard_1>} ></Route> */}


   </Routes>
   </BrowserRouter>
   </>
  );
}

export default App;
